﻿using UnityEngine;
using System.Collections;

public class PowerUpTracker : DefaultTrackableEventHandler {
	
	public GameObject powerUp;
	private rotador rotar;
	
	// Use this for initialization
	//NO COLOCAR START() 
	void Awake () {
		//aparicion = gameObject.GetComponentInChildren <MovAparicion> ();
		rotar = powerUp.GetComponent<rotador> ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	/// <summary>
	/// Cuando encuetras el Track, activa la aparicion del fantasma.
	/// </summary>
	protected override void OnTrackingFound()
	{
		base.OnTrackingFound();
		//no se activara si ya aparecio el fantasma actual, y no se ha generado el siguiente.
		if (rotar) {
			rotar.setRotar(true);
		}
		
	}
	
	/// <summary>
	/// Cuando pierde el Track, interrumpe la aparicion si no ha terminado.
	/// </summary>
	protected override void OnTrackingLost()
	{
		base.OnTrackingLost();

	}
	
}
