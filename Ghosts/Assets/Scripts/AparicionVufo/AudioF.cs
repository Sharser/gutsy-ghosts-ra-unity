﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Esta clase Controla el manejo del efecto de sonido que genera el fantasma cuando 
/// aparece y cuando gira alrededor del fantasma.
/// </summary>
public class AudioF : MonoBehaviour {

	//Variables Publicas
	public float intervalo;						//Tiempo, en segundos, que el fantasma se mantiene sonando.
	public float intervaloMute;					//Tiempo, en segundos, que el fantasma se mantiene en silencio.
	public float maxVol;						//Volumen maximo del audioSourse, valores entre 0.0f y 1.0f.

	//Variables Privadas
	private bool activar;						//Booleano que indica si se activan o no los efectos de sonido.
	private bool accion;						//Booleano que indica si ya se activo el efecto y evita que se active repetidamente.
	private bool muteI;							//Booleano que indica si se encuntra en el intervalo de mute (true) o del sonido (false).
	private float count;						//Variable auxiliar para contar el tiempo transcurrido.
	private AudioSource audioS;					//Componente AudioSSource que genera el sonido 3D.

	// Use this for initialization
	void Start () {
		muteI = false;
		accion = false;
		audioS = this.gameObject.GetComponent<AudioSource>();
		if (audioS) {
			audioS.playOnAwake = false;
			audioS.clip = (AudioClip)Resources.Load ("audio/HORRO21");
			audioS.volume = maxVol;
			audioS.maxDistance = 1000;
		} else {
			Debug.LogError("No se encontro el componente AudioSource");
		}
	}
	
	// Update is called once per frame
	void Update () {
		//Si estan activos los efectos de sonido.
		if (activar) {
			//Intervalo de mute.
			if(muteI){

				//Si no se ha parado el sonido en este intervalo.
				if(!accion){
					//Debug.Log("stop");
					StopClip();
					accion = true;
				}

				//Mientras no se termine el intervalo.
				if(count < intervaloMute){
					count += Time.deltaTime;
					//Debug.Log("mute: "+count.ToString());
				//Si se termino el intervalo
				}else{
					muteI = false;
					count = 0f;
					accion = false;
				}

			//Intervalo de sonido.
			}else{

				//Si no se ha reproducido el sonido en este intervalo.
				if(!accion){
					//Debug.Log("play");
					PlayClip();
					accion = true;
				}

				//Mientras no se termine el intervalo
				if(count < intervalo){
					count += Time.deltaTime;
					//Debug.Log("play: "+count.ToString());
				//Si se termino el intervalo
				}else{
					muteI = true;
					count = 0f;
					accion = false;
				}
			}
		}
	}

	/// <summary>
	/// Activar el efecto del fantasma cuando gira alrededor del jugador.
	/// </summary>
	public void Activate(){
		audioS.clip = (AudioClip) Resources.Load ("audio/HORRO21");
		audioS.loop = true;
		activar = true;
		muteI = false;
		accion = false;
	}

	/// <summary>
	/// Desactivar el efecto del fantasma cuando gira alrededor del jugador..
	/// </summary>
	public void Desactivate(){
		activar = false;
	}

	/// <summary>
	/// Reproducir el Clip de audioS actual.
	/// </summary>
	public void PlayClip(){
		audioS.Play ();
	}
	/// <summary>
	/// Parar de reproducir el Clip de audioS actual.
	/// </summary>
	public void StopClip(){
		audioS.Stop ();
	}

}
