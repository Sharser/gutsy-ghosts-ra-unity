﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Esta clase maneja la reaparicion de los fantasmas usando un mismo ImageTracker de Vuforia.
/// </summary>
public class ContadorAparicion : MonoBehaviour {

	//Variables Publicas
	public float timeAparicion;					//Tiempo, en segundos, para que se genere el proximo fantasma.
	public GameObject ghost;					//Referencia al Prefab del fantasma.
	public Transform init;						//Referencia al Transform "GhostPosIni", el cual guarda la posicion y la rotacion inicial.
	public Transform padre;						//Referencia al Transform "Fantasmas" que mantiene a todos los fantasmas generados despues de que aparecen.

	//Variables Privadas
	private bool aparecer;						//Indica si ya se puede generar el proxima fantasma o no.
	private float count;						//Variable auxiliar para contar el tiempo transcurrido.
	private ghostTracker gtracker;				//Componente ghostTracker del ImageTracker, que maneja la aparicion de los fantasmas.
	private MovAparicion aparicion;				//Componente MovAparicion del fantasma actual.
	private GameObject currentGhost;			//Referencia a la instancia actual del fantasma.

	// Use this for initialization
	void Awake () {
		count = 0.0f;
		aparecer = true;
		gtracker = gameObject.GetComponent<ghostTracker> ();
		respawn ();
	}
	
	// Update is called once per frame
	void Update () {
		//Si el fantasma actual ya aparecio, comenzar.
		if(!aparecer){
			//Se espera hasta que se cumpla el tiempo para instanciar el proximo.
			if(count < timeAparicion){
				count += Time.deltaTime;
			//Si se cumple el tiempo se instancia
			}else{
				aparecer = true;
				respawn();
			}
		}
	}

	/// <summary>
	/// Indica que el fantasma actual ya aparecio, por lo que reinicia los valores de la clase.
	/// </summary>
	public void reinicio(){
		aparecer = false;
		count = 0.0f;
		gtracker.aparicion = null;
		gtracker.currentGhost = null;
	}

	/// <summary>
	/// Genera una nueva instacia de un fantasma en inicializa sus referencias.
	/// </summary>
	public void respawn(){

		if(ghost && init && padre){
			currentGhost = (GameObject)Object.Instantiate (ghost, init.position, init.rotation);
			currentGhost.SetActive(false);
			currentGhost.transform.parent = this.transform;
			aparicion = currentGhost.GetComponent <MovAparicion> ();
			aparicion.padreFantasma = padre;
		}else{
			if(!ghost){
				Debug.LogError("No se coloco la refenrenca al Prefab del fantasma");
			}
			if(!init){
				Debug.LogError("No se coloco la refenrenca al Transform que posee al posicion y rotacion inicial");
			}
			if(!padre){
				Debug.LogError("No se coloco la refenrenca al objeto contenedor de los fantasmas");
			}
		}

		if(gtracker){
			gtracker.aparicion = aparicion;
			gtracker.currentGhost = currentGhost;
		}else{
			Debug.LogError("No se encontro el componente ghostTracker");
		}
	}



}
