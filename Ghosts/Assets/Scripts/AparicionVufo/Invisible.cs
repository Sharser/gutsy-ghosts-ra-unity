﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clase que controla la transparencia de los fantasma en el juego.
/// </summary>
public class Invisible : MonoBehaviour {

	public float spd = 1.0f;					//Velocidad con la que cambia el canal alfa del material del fantasma.
	public float invTime = 3.0f;				//Tiempo que el fantasma se mantendra invisible.
	public float minTransparencia = 0.0F;		//El minimo valor (entre 0 y 1) que el canal alfa puede tener al hacerce invisible.

	private bool aumentar;						//Booleano que indica si el valor del canal alfa aumentara o disminuira.
	private float contador;						//Variable auxiliar medir el aumento o la disminucion del valor del canal alfa.
	private float contadorTime;					//Variable auxiliar para contar el tiempo transcurrido.
	[SerializeField]
	private Transform[] hijos;					//Arreglo estatico con los objeros hijos del fantasma que se haran invisibles.

	// Use this for initialization
	void Start(){
		aumentar = false;
		contador = 1.0f;
		contadorTime = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		//Si el fantasma se hara visible.
		if (aumentar) {
			contador += Time.deltaTime * spd;

			//Si es visible totalmente. 
			if (contador > 1) {
				contador = 1;
				aumentar = false;
			}

		//Si el fantasma se hara invisible.
		} else {
			contador -= Time.deltaTime * spd;

			//Si el fantasma es lo minimo visible.
			if (contador < minTransparencia) {
				contador = minTransparencia;
				contadorTime += Time.deltaTime;

				//Si se cumplio el tiempo de invisibilidad.
				if(contadorTime > invTime){ 
					aumentar = true;
					contadorTime = 0f;
				}
			}
		}

		//Le coloca el valor del canal alfa a todos los objetos hijos del fantasma.
		foreach (Transform h in hijos) {
			Material[] mats = h.gameObject.renderer.materials;
			foreach (Material m in mats) {
				Color mcolor = m.color;
				mcolor.a = contador;
				m.color = mcolor;
			}
		}

	}
}
