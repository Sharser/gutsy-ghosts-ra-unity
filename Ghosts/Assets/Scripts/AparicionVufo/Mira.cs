﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clase que se encarga del comportamiento de la mira.
/// </summary>
public class Mira : MonoBehaviour {

	//Variables Publicas
	public float spd;							//Velocidad con la que rota la mira al enfocar.
	public float porcReduc;						//Valor (entre 0 y 1), que indica cual es la escala minima que la mira se reducira.
	public Material mat0;						//Material inicial transparente de la mira.
	public Material mat1;						//Material que indica que el fantasma se esta enfocando.
	public Material mat2;						//Material que indica que el fantasma esta enfocado y que se puede cazar.

	//Variables Privadas
	[SerializeField]
	private bool activo;						//Booleano que indica que la mira esta activa.
	private float time;							//Tiempo que tarda en reducirse la mira de su escala original a la escala minima.
	private float scale;						//Escala original de la mira.
	private float count;						//Variable auxiliar para contar el tiempo transcurrido.
	private float scaleReduc;					//Variable auxiliar, que seria la escala minima correspondiente al porcReduc.
	private float spdAux;						//Variable auxiliar que corresponde a la velocidad de reduccion.

	// Use this for initialization
	void Start(){
		activo = false;
		scale = this.transform.localScale.x;
		scaleReduc = scale * porcReduc;
		spdAux = scaleReduc/time;
		this.renderer.enabled = false;
		this.renderer.material = mat0;

	}

	// Update is called once per frame
	void Update () {
		//Si la mira se activa, por tocar el fantasma en la pantalla.
		if (activo) {
			girar();

			//Si no se ha cumplido el tiempo de enfoque.
			if(count < time){
				this.transform.localScale -= Time.deltaTime*spdAux*new Vector3(1,0,1);
				count += Time.deltaTime;

			//Si se cumplio el tiempo de enfoque.
			}else{
				this.renderer.material = mat2;
			}
		}
	}
	/// <summary>
	/// Reinicia la mira en caso de que se pierda el enfoque del fantasma.
	/// </summary>
	public void reinicio(){
		count = 0;
		this.renderer.material = mat1;
		this.transform.localScale = new Vector3(scale, 1, scale);
	}

	/// <summary>
	/// Activa la mira.
	/// </summary>
	public void activarGiro(){
		activo = true;
		this.renderer.enabled = true;
	}

	/// <summary>
	/// Desactiva la mira.
	/// </summary>
	public void desactivarGiro(){
		activo = false;
		this.renderer.enabled = false;
	}

	/// <summary>
	/// Setea el tiempo del enfoque.
	/// </summary>
	/// <param name="t">Tiempo de enfoque.</param>
	public void setTime(float t){
		time = t;
	}
	
	/// <summary>
	/// Girar la mira sobre si misma.
	/// </summary>
	private void girar(){
		transform.Rotate(Vector3.up, spd * Time.deltaTime, Space.Self);
	}
}
