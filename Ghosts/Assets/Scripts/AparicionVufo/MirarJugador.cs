﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clase que hace que el objeto se mantenda de frente al jugador.
/// </summary>
public class MirarJugador : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.LookAt (Vector3.zero);
	}
}
