﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clase que se encarga del movimiento del fantasma alrededor del jugador, tanto horizontal como verticalmente.
/// </summary>
public class MovAlrededor : MonoBehaviour {

	//Variables Publicas
	public float spdMax;						//Velocidad maxima que el fantasma puede girar alrededor del jugador.
	public float spdMin;						//Velocidad minima que el fantasma puede girar alrededor del jugador.
	public float timeMax;						//Tiempo maximo que el fantasma puede girar alrededor del jugador en un sentido antes de cambiar al otro.
	public float timeMin;						//Tiempo minimo que el fantasma puede girar alrededor del jugador en un sentido antes de cambiar al otro.
	public float spdRotar;						//Velocidad con la que el fantasma rota para ver al jugador cuando termina de aparecer.
	public float spdVerMax;						//Velocidad maxima que el fantasma subir o bajar alrededor del jugador.
	public float spdVerMin;						//Velocidad minima que el fantasma subir o bajar alrededor del jugador.
	public float timeVerMax;					//Tiempo maximo que el fantasma puede subir o bajar alrededor del jugador antes de cambiar de direccion.
	public float timeVerMin;					//Tiempo maximo que el fantasma puede subir o bajar alrededor del jugador antes de cambiar de direccion.
	public Transform miraJugador;				//Tiempo maximo que el fantasma puede subir o bajar alrededor del jugador antes de cambiar de direccion.
	
	//Variables Privadas
	private bool mirando;						//Booleano que indica si el fantasma esta mirando actualmente al jugador.
	private float count; 						//Variable auxiliar que lleva el tiempo de cuanto se a movido el fantasma en un sentido alrdedor del jugador.
	private float countVer;  					//Variable auxiliar que lleva el tiempo de cuanto ha subido o bajado el fantasma alrdedor del jugador.
	private float spd;							//Velocidad con la que el fantasma gira alrededor del jugador en un intervalo.
	private float time;							//Tiempo de un intervalo en el que el fantasma gira alrededor del jugador.
	private Vector3 ejeGiro;					//Eje respecto al cual el fantasma gira en sentido de las agujas del relog, puede ser Vector3.up o Verctor3.down.
	private Vector3 DirVer;						//Direccion que indica si el fantasma se mueve hacia arriba o hacia abajo, puede ser Vector3.up o Verctor3.down.
	private float spdVertical;					//Velocidad con la que el fantasma sube o baja alrededor del jugador en un intervalo.
	private float timeVertical;					//Tiempo de un intervalo en el que el fantasma sube o baja alrededor del jugador.
	private float gradoMirada;					//Variable auxiliar que contiene el porcentaje de rotacion del fantasma al mirar al jugador.
	private AudioF audios;						//Componente AudioF que controla los sonidos 3D.

	// Use this for initialization
	void Awake(){
		count = 0;
		countVer = 0;
		gradoMirada = 0;
		mirando = false;
	}

	void Start(){
		ejeGiro = randomEje ();
		DirVer = randomEje ();
		spd = Random.Range(spdMin, spdMax);
		time = Random.Range(timeMin, timeMax);
		spdVertical = Random.Range(spdVerMin, spdVerMax);
		timeVertical = Random.Range(timeVerMin, timeVerMax);
		audios = gameObject.GetComponent<AudioF> ();
		audios.Activate ();
	}
	// Update is called once per frame
	void Update () {

		//ESTA SECCION SE ENCARGA DE QUE EL FANTASMA MIRE AL JUGADOR.
		//Mientras el fantasma no este girando de frente al jugador.
		if (!mirando) {
			mirar();

		//Si ya fantasma esta mirando al jugador, hay que mantenerlo mientras gira.
		}else{
			//El jugador esta en el origen del mundo;
			this.transform.LookAt (Vector3.zero);
		}


		//ESTA SECCION SE ENCARGA DEL MOVIMIENTO ALREDEDOR DEL JUGADOR.

		//Mientras se mantenga el tiempo del intervalo actual.
		if (count < time) {
			count += Time.deltaTime;

		//Cuando termina un intervalo.
		}else{
			count = 0.0f;
			if(ejeGiro == Vector3.up){
				ejeGiro = Vector3.down;
			}else{
				ejeGiro = Vector3.up;
			}
			spd =  Random.Range(spdMin, spdMax);
			time = Random.Range(timeMin, timeMax);
		}

		transform.RotateAround(Vector3.zero, ejeGiro, spd * Time.deltaTime);


		//ESTA SECCION SE ENCARGA DEL MOVIMIENTO VERTICAL.

		//Mientras se mantenga el tiempo del intervalo actual.
		if (countVer < timeVertical) {
			countVer += Time.deltaTime;

		//Cuando termina un intervalo.
		}else{
			countVer = 0.0f;
			if(DirVer == Vector3.up){
				DirVer = Vector3.down;
			}else{
				DirVer = Vector3.up;
			}
			spdVertical =  Random.Range(spdVerMin, spdVerMax);
			timeVertical = Random.Range(timeVerMin, timeVerMax);
		}
		this.transform.Translate(DirVer*spdVertical*Time.deltaTime, Space.Self);
	}

	/// <summary>
	/// Rota el fantasma suavemente hacia el jugador.
	/// </summary>
	private void mirar(){
		if(gradoMirada < 1.0f){
			gradoMirada += spdRotar*Time.deltaTime;
		}else{
			mirando=true;
		}
		this.transform.rotation = Quaternion.Slerp(this.transform.rotation, miraJugador.rotation, gradoMirada);
	}

	/// <summary>
	/// Devuelve Vector3.up o Vector3.down con 50% de probabilidad.
	/// </summary>
	/// <returns>Eje o direccion</returns>
	private Vector3 randomEje(){
		if(Random.value < 0.5){
			return Vector3.up;
		}else{
			return Vector3.down;
		}
	}

}



