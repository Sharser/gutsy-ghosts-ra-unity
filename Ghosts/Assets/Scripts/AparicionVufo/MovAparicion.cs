﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Clase que controla la aparicion del fantasma, ademas activa los otros componentes del fantasma,
/// y finalmente controla el acercamiento del fantasma al jugador despues de que aparece.
/// </summary>
public class MovAparicion : MonoBehaviour {

	//Variables publicas;
	public float spd;							//Velocidad con la que el fantasma saldra de la imagen.
	public float spdAcerca;						//Velocidad con la que el fantasma se acercara al jugador.
	public float timeAni;						//Tiempo que durara el fantasma saliendo de la imagen.
	public float distancia;						//Distancia minima entre el fantasma y el jugador.
	public float timeAparicionAu;				//Tiempo que durara el sonido de la aparicion.
	public Vector3 dir;							//Direccion a donde se dirijira el fantasma, en este caso es el jugador (con posicion (0,0,0)).
	public Transform padreFantasma;				//Referencia al Transform "Fantasmas" que mantiene a todos los fantasmas generados despues de que aparecen.

	//Variables privadas
	[SerializeField]
	private bool activate;						//Booleano que indica se activa la aparicion del fantasma y desencadena la activacion de los demas componentes.
	private bool acercarse;						//Boolenao que indica si el fantasma termino de aparecer y se acerca al jugador.
	private bool sonoAparicion;					//Booleano que indica si ya se activo el sonido de aparicion.
	private float timeCount;					//Variable auxiliar para contar el tiempo transcurrido.		
	private float distMag;						//Es la magnidad del vector que indica la distancia entre el fantasma y el jugador.
	private Touch tocar;						//Componente Touch que controla la interaccion del jugador a traves de la pantalla del jugador.
	private AudioF audios;						//Componente AudioF que controla los sonidos 3D.
	private Invisible inv;						//Componente Invisible que maneja la transparencia del fantasma.
	private MovAlrededor alrededor;				//Componente MovAlrededor que controla el movimiento erratico del fantasma.
	private ContadorAparicion contadorPadre = null;	//Componente que se encarga del respawn de fantasma en el tracker que aparecio.

	// Use this for initialization
	void Awake () {
		timeCount = 0.0f;
		activate = false;
		acercarse = false;
		sonoAparicion = false;
	}

	void Start(){
		alrededor = gameObject.GetComponent<MovAlrededor> ();
		inv = gameObject.GetComponent<Invisible> ();
		audios = gameObject.GetComponent<AudioF> ();
		tocar = gameObject.GetComponent<Touch> ();
		contadorPadre = this.transform.parent.gameObject.GetComponent<ContadorAparicion> ();
		tocar.enabled = false;
		alrededor.enabled = false;
		inv.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {

		//ESTA SECCION SE ENCARGA DE LA ANIMACION DE APARICION.
		//Mientras dure la animacion.
		if (activate && (timeCount < timeAni)) {
			this.transform.Translate(new Vector3(dir.x,dir.y,dir.z)*spd*Time.deltaTime);
			timeCount += Time.deltaTime;

			//El fantasma emite un sonido durante la aparicion
			if(!sonoAparicion && (timeCount > timeAparicionAu)){
				audios.PlayClip();
				sonoAparicion = true;
			}
		} 

		//ESTA SECCION SE ENCARGA DE DESACTIVAR LA ANIMACION,
		//DE ACTIVAR LOS OTROS COMPONENTE, INICIAR EL ACERCAMIENTO
		//AL JUGADOR Y DESVINCULARSE DEL TRACKER.

		//Si se termino la animacion y no se ha acercado
		if (timeCount >= timeAni && !acercarse) {
			activate = false;
			acercarse = true;
			if(padreFantasma)
				this.transform.parent = padreFantasma;
			if(contadorPadre)
				contadorPadre.reinicio();
			if(alrededor)
				alrededor.enabled = true;
			if(inv)
				inv.enabled = true;
			if(tocar)
				tocar.enabled = true;
			
			distMag = (this.transform.position).magnitude;
		}

		//ESTA PARTE SE ENCARGA DE ACERCARCE AL JUGADOR

		//Mientras me acerco al jugador
		if(acercarse){
			if(distancia < distMag){
				distMag = (this.transform.position).magnitude;
				this.transform.Translate(new Vector3(-this.transform.position.x, -this.transform.position.y, -this.transform.position.z)*(1.0f*spdAcerca*Time.deltaTime/distMag),Space.World);
			}else{
				acercarse = false;
				this.enabled = false;
			}
		}
	}

	/// <summary>
	/// Activa la aminacion de la aparicion de la animacion si no se ha termido.
	/// </summary>
	public void ActivateAni(){
		activate = true;
	}
	/// <summary>
	/// Desactiva la aminacion de la aparicion de la animacion si no se ha termido.
	/// </summary>
	public void DesactivateAni(){
		activate = false;
	}
}
