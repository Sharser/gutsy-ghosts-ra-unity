﻿using UnityEngine;
using System;
using System.Collections;
using TouchScript.Gestures;

/// <summary>
/// Clase que se encarga del uso de Plugin de TouchScripts, para el manejo de la pantalla tactil para esta escena.
/// </summary>
public class Touch : MonoBehaviour {

	//Variable Publicas
	public float time;							//Tiempo que dura el enfoque del fantama.
	public float timeHold;						//Tiempo que se puede mantener enfocado el fantasma antes de que se pierda.

	//Variables Privadas
	private bool contar;						//Booleano que indica si se esta contando el tiempo de enfoque.
	private bool exorcitar;						//Booleano que indica si se puede eliminar el fantasma
	private float count;						//Variable auxiliar para contar el tiempo transcurrido antes de enfoque.	
	private float countHold;					//Variable auxiliar para contar el tiempo transcurrido durante en enfoque antes de que se pierda.
	private Mira miraCamara;					//Componente Mira del objeto "Mira" que enfoca al fantasma.
	private TapGesture tap;						//Componente TapCamara que controla los taps en la pantalla.
	private PressGesture press;					//Componente PressGesture que controla cuando se presona la pantalla.
	private ReleaseGesture release;				//Componente ReleaseGesture que controla cuando se suelta la pantalla.


	// Use this for initialization
	void Awake () {
		count = 0.0f;
		countHold = 0.0f;
		//permitir = false;
		tap = GetComponent<TapGesture>();
		press = GetComponent<PressGesture>();
		release = GetComponent<ReleaseGesture>();
		Transform mirarT = transform.FindChild("Mira");
		miraCamara = mirarT.gameObject.GetComponentInChildren<Mira> ();
		miraCamara.setTime(time);
	}
	
	// Update is called once per frame
	void Update () {
		//mientras se trata de enfocar al fantasma
		if(contar && !exorcitar){
			if (count < time) {
				count += Time.deltaTime;
			}else{
				exorcitar = true;
			}
		}

		//mientras esta enfocado
		if(exorcitar){
			if (countHold < timeHold) {
				countHold += Time.deltaTime;
			}else{
				exorcitar = false;
				reinicio ();
			}
		}
	}

	/// <summary>
	/// Reinicia los valores del enfoque de la mira y de esta clase.
	/// </summary>
	private void reinicio(){
		count = 0.0f;
		countHold = 0.0f;
		miraCamara.desactivarGiro();
		miraCamara.reinicio();
		
	}

	/// <summary>
	/// Asigna los Handlers de los gestos de touch.
	/// </summary>
	void OnEnable () {
		tap.Tapped += tappedHandler;
		press.Pressed += pressedHandler;
		release.Released += releasedHandler;
	}

	/// <summary>
	/// Quita los Handlers de los gestos de touch.
	/// </summary>
	void OnDisable () {
		tap.Tapped -= tappedHandler;
		press.Pressed -= pressedHandler;
		release.Released -= releasedHandler;
	}

	/// <summary>
	/// Handler de Tap.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	private void tappedHandler(object sender, EventArgs e)
	{
		if (exorcitar) {
			Destroy(gameObject);
		}
	}

	/// <summary>
	/// Handler de Press.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	private void pressedHandler(object sender, EventArgs e)
	{
		contar = true;
		miraCamara.activarGiro ();
	}

	/// <summary>
	/// Handler de Release.
	/// </summary>
	/// <param name="sender">Sender.</param>
	/// <param name="e">E.</param>
	private void releasedHandler(object sender, EventArgs e)
	{
		contar = false;
		if (!exorcitar) {
			reinicio();
		}
	}



}
