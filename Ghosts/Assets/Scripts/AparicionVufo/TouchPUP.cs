﻿using UnityEngine;
using System;
using System.Collections;
using TouchScript.Gestures;

public class TouchPUP : MonoBehaviour {
	
	private bool destruir;
	[SerializeField]
	private TapGesture tap;						//Componente TapCamara que controla los taps en la pantalla.
	// Use this for initialization
	void Awake () {
		tap = GetComponent<TapGesture>();
		destruir = true;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnEnable () {
		tap.Tapped += tappedHandler;
	}
	
	/// <summary>
	/// Quita los Handlers de los gestos de touch.
	/// </summary>
	void OnDisable () {
		tap.Tapped -= tappedHandler;
	}
	
	private void tappedHandler(object sender, EventArgs e)
	{
		if(destruir){
			Destroy (this.gameObject);
		}
	}
	/*private void pressedHandler(object sender, EventArgs e)
	{
		Debug.Log ("press");
	}*/
	
	public void setDestruir(bool dest){
		destruir = dest;
	}
}
