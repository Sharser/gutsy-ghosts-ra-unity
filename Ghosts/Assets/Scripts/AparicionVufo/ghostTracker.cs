using UnityEngine;
using System.Collections;

/// <summary>
/// Subclase de DefaultTrackableEventHandler, que se encarga de la activacion de los fantasmas
/// cuando el tracker detecta el objetivo.
/// </summary>
public class ghostTracker : DefaultTrackableEventHandler {

	public MovAparicion aparicion = null;
	public GameObject currentGhost = null;

	// Use this for initialization
	//NO COLOCAR START() 
	void Awake () {
		//aparicion = gameObject.GetComponentInChildren <MovAparicion> ();
	}

	// Update is called once per frame
	void Update () {
	}

	/// <summary>
	/// Cuando encuetras el Track, activa la aparicion del fantasma.
	/// </summary>
	protected override void OnTrackingFound()
	{
		base.OnTrackingFound();
		//no se activara si ya aparecio el fantasma actual, y no se ha generado el siguiente.
		if (currentGhost && aparicion) {
			currentGhost.SetActive (true);
			aparicion.ActivateAni ();
		}

	}

	/// <summary>
	/// Cuando pierde el Track, interrumpe la aparicion si no ha terminado.
	/// </summary>
	protected override void OnTrackingLost()
	{
		base.OnTrackingLost();
		//no se deactivara si ya aparecio el fantasma actual, y no se ha generado el siguiente.
		if(currentGhost && aparicion){
			currentGhost.SetActive(false);
			aparicion.DesactivateAni();
		}

	}

}
