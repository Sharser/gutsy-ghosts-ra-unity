﻿using UnityEngine;
using System.Collections;

public class rotador : MonoBehaviour {

	private bool rotar;
	private float spd;

	void Start () {
		spd = 1f;
		rotar = false;
	}

	// Update is called once per frame
	void Update () {
		if(rotar){
			transform.Rotate(new Vector3 (15, 30, 45)*spd*Time.deltaTime);
		}
	}

	
	public void setRotar(bool rot){
		rotar = rot;
	}
}
