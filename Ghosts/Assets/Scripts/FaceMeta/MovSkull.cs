﻿using UnityEngine;
using System.Collections;

public class MovSkull : MonoBehaviour {

	public GameObject tracker;

	private Vector3 initPos;
	[SerializeField]
	//private bool pauseDebug;
	private metaioTracker metatracker;

	// Use this for initialization
	void Awake () {
		//pauseDebug = true;
		//initPos = this.transform.position;
		initPos = tracker.transform.position;
		metatracker = tracker.GetComponent<metaioTracker> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*if(Input.GetKeyDown(KeyCode.Space)){
			pauseDebug = !pauseDebug;
		}*/
	}

	public void RestartPos(){
		this.transform.position = initPos;
	}

	public bool onTrack(){
		//return pauseDebug;
		return (metatracker.isTracking > 0);
	}
}
