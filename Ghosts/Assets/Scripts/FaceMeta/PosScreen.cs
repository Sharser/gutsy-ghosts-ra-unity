﻿using UnityEngine;
using System.Collections;

public class PosScreen : MonoBehaviour {

	public Camera cam;
	public Vector3 pos;
	// Use this for initialization
	void Start () {
		pos = cam.WorldToScreenPoint (this.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log ("x= "+pos.x.ToString()+", y= "+pos.y.ToString()+", z= "+pos.z.ToString());
	}
}
