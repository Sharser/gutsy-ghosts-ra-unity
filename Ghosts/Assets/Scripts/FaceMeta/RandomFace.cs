﻿using UnityEngine;
using System.Collections;

public class RandomFace : MonoBehaviour {

	public float timeMin;
	public float timeMax;
	public float timeMinInv;
	public float timeMaxInv;
	public GameObject ghostSkull;

	private bool aparece;
	private float count;
	private float timeI;
	private MovSkull mskull;
	private metaioTracker metatracker;
	// Use this for initialization
	void Start () {
		aparece = false;
		restartCount ();
		mskull = ghostSkull.GetComponent<MovSkull>();
		metatracker = this.GetComponent<metaioTracker> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(metatracker.isTracking > 0){
			count += Time.deltaTime;
			if(count > timeI){
				if(aparece){
					mskull.RestartPos();
					ghostSkull.SetActive(false);
				}else{
					ghostSkull.SetActive(true);
				}

				restartCount();
			}
		}else{
			if(aparece){
				count = 0.0f;
			}else{
				count += Time.deltaTime;
			}
		}
	}

	private void restartCount(){
		count = 0.0f;
		aparece = !aparece;
		if(aparece)
			timeI = Random.Range (timeMin, timeMax);
		else
			timeI = Random.Range (timeMinInv, timeMaxInv);
	}

	public void atrapar(){
		if(aparece){
			mskull.RestartPos();
			ghostSkull.SetActive(false);
			restartCount ();
		}
	}

}
