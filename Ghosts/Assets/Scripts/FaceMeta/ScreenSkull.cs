﻿using UnityEngine;
using System.Collections;

public class ScreenSkull : MonoBehaviour {

	const string tagFantasma = "Ghost";
	public float relativeHeight;
	public float relativeWight;
	public float boxHeightRel = 0.13f;
	public float boxWidthRel = 0.13f;
	public GameObject ghostSkull;

	//private Ray raysk;
	//private RaycastHit hitsk;
	[SerializeField]
	private float height;
	[SerializeField]
	private float width;
	private float boxHeight;
	private float boxWidth;
	private Vector3 pos;
	private MovSkull mskull;

	// Use this for initialization
	void Start () {
		height = Screen.height;
		width = Screen.width;
		boxHeight = boxHeightRel * height;
		boxWidth = boxWidthRel * width;
		mskull = ghostSkull.transform.GetComponent<MovSkull>();
	}

	
	// Update is called once per frame
	void Update () {
		pos = this.camera.WorldToViewportPoint (ghostSkull.transform.position);
		//raysk = this.camera.ScreenPointToRay (new Vector3(width*relativeWight,height*relativeHeight,this.camera.farClipPlane));
		//Debug.DrawLine (this.camera.ViewportToWorldPoint (new Vector3 (relativeWight, relativeHeight, 0)), 
		//				  this.camera.ViewportToWorldPoint (new Vector3 (relativeWight, relativeHeight, 0))+ this.camera.transform.forward * 1000,
		//                Color.red);
		//Debug.DrawLine (this.transform.position, this.transform.position+this.transform.forward * 1000);
		/*if(Physics.Raycast(raysk, out hitsk)){
			if(hitsk.collider.gameObject.tag == tagFantasma){
				Debug.Log("Hola");
				MovSkull mskull = hitsk.transform.GetComponent<MovSkull>();
				Destroy(hitsk.transform.gameObject);
				mskull.RestartPos();
			}
		}*/
		Debug.Log (pos);
		if(relativeWight < pos.x && pos.x < relativeWight+boxWidthRel){
			if(relativeHeight < pos.y && pos.y < relativeHeight+boxHeightRel){
				mskull.RestartPos();
			}
		}


	}

	void OnGUI()
	{
		GUI.Box (new Rect (width*relativeWight, height*(1-relativeHeight)-boxHeight, boxWidth, boxHeight), "hola");
	}
}
