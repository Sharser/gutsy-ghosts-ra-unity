﻿using UnityEngine;
using System.Collections;
using System;
using TouchScript.Behaviors;
using TouchScript.Gestures;
using TouchScript.Gestures.Simple;

public class TouchFace : MonoBehaviour {

	private bool lost;
	[SerializeField]
	private MovSkull mskull;
	[SerializeField]
	private SimplePanGesture simplePan;
	[SerializeField]
	private Transformer2D trans2D;


	// Use this for initialization
	void Awake () {
		lost = false;
		mskull = GetComponent<MovSkull> ();
		trans2D = GetComponent<Transformer2D> ();
		simplePan = GetComponent<SimplePanGesture>();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnEnable () {
		simplePan.PanStarted += BeginPanHandler;
		simplePan.Panned += PannedHandler;
		simplePan.PanCompleted += EndPanHandler;
	}

	void OnDisable () {
		simplePan.PanStarted -= BeginPanHandler;
		simplePan.Panned -= PannedHandler;
		simplePan.PanCompleted -= EndPanHandler;
	}
	
	private void BeginPanHandler(object sender, EventArgs e){
		if (lost) {
			trans2D.enabled = true;
		}
	}

	private void PannedHandler(object sender, EventArgs e){
		if (!mskull.onTrack ()) {
			mskull.RestartPos ();
			lost = true;
			trans2D.enabled = false;
		}
	}

	private void EndPanHandler(object sender, EventArgs e){
		mskull.RestartPos ();
	}
}
