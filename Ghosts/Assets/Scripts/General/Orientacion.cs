﻿using UnityEngine;
using System.Collections;

public class Orientacion : MonoBehaviour {

	public bool portrait;
	public bool landscape;
	// Use this for initialization
	void Awake () {
		if(portrait){
			Screen.orientation = ScreenOrientation.Portrait;
		}
		if(landscape){
			Screen.orientation = ScreenOrientation.Landscape;
		}
		Screen.autorotateToLandscapeLeft = landscape;
		Screen.autorotateToLandscapeRight = landscape;
		Screen.autorotateToPortrait = portrait;
		Screen.autorotateToPortraitUpsideDown = portrait;
	}
	
	// Update is called once per frame
	void Update () {
	}
}
