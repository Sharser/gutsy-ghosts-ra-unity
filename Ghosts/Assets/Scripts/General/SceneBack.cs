﻿using UnityEngine;
using System.Collections;

public class SceneBack : MonoBehaviour {

	public float heightRel = 0.13f;
	public float widthRel = 0.125f;
	
	private float posH=0.0f;
	private float posW;
	private float height;
	private float width;
	private float botonh;
	private float botonw;
	
	void Start(){
		height = Screen.height;
		width = Screen.width;
		posW = 1 - widthRel;
		botonh = height * heightRel;
		botonw = width * widthRel;
		
	}
	void OnGUI()
	{

		if (GUI.Button(new Rect(posW*width, posH*height, botonw, botonh), "Back"))
		{
			if(Input.location.status == LocationServiceStatus.Running ||
			   Input.location.status == LocationServiceStatus.Initializing){
				Input.location .Stop();
			}
			Application.LoadLevel (0);
		}

	}
}
