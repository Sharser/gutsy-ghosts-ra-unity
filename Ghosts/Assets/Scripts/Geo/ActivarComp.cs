﻿using UnityEngine;
using System.Collections;

public class ActivarComp : MonoBehaviour {
	
	private MovAlrededorGeo movAlr;
	private Invisible inv;
	private Touch toque;
	// Use this for initialization
	void Start () {
		inv = this.gameObject.GetComponent<Invisible> ();
		movAlr = this.gameObject.GetComponent<MovAlrededorGeo> ();
		toque = this.gameObject .GetComponent<Touch> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Actives todos los componentes de interes.
	/// </summary>
	/// <param name="setActive">If set to <c>true</c> set enable in all components.</param>
	public void activeAllComp(bool setActive){
		inv.enabled = setActive;
		movAlr.enabled = setActive;
		toque.enabled = setActive;
	}
}
