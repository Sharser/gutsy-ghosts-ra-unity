﻿using UnityEngine;
using System.Collections;

public class Compass : MonoBehaviour {

	public Transform target;

	private Vector3 dir;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Quaternion q = transform.parent.transform.rotation;
		transform.parent.transform.rotation = Quaternion.Euler(Vector3.zero);
		dir = target.position - this.transform.parent.transform.position;
		this.transform.LookAt(dir);
		transform.parent.transform.rotation = q;
		/*
		dir = target.position - this.transform.position;
		dirlocal = ProjectVectorOnPlane (this.transform.up, dir);
		Debug.Log (this.transform.up);
		Debug.Log(Vector3.Dot(dir, this.transform.up));
		this.transform.LookAt (dirlocal, this.transform.up);
		*/

	}
}