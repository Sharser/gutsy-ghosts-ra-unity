﻿using UnityEngine;
using System.Collections;

public class DebugSlider : MonoBehaviour {

	public float posPruebax;
	public float posPruebay;
	public float relativeSliderH;
	public float relativeSliderW;
	public float relSliderPosH;
	public float relSliderPosW;

	private float width;
	private float height;
	private float sliderValx;
	private float sliderValy;
	private metaioTracker tracker;
	private LocationInfo posCel;
	// Use this for initialization
	void Awake(){
		Input.location.Start (10f,10f);
	
	}

	void Start () {
		tracker = this.GetComponent<metaioTracker> ();
		width = Screen.width;
		height = Screen.height;
		posPruebax = tracker.geoLocation.x;
		posPruebay = tracker.geoLocation.y;
		sliderValx = tracker.geoLocation.x;
		sliderValy = tracker.geoLocation.y;
		posCel = Input.location.lastData;

	}
	
	// Update is called once per frame
	void Update () {
		tracker.geoLocation.x = sliderValx;
		tracker.geoLocation.y = sliderValy;
	}

	void OnGUI(){
		//float hSliderValue = 0;
		//sliderValx = GUI.HorizontalSlider(new Rect(width * relSliderPosW, height * relSliderPosH, relativeSliderW, 30), sliderValx, 0.0F, 10.0F);
		sliderValx = GUI.HorizontalSlider (new Rect (width * relSliderPosW, height * relSliderPosH, relativeSliderW*width, relativeSliderH*height),
		                                   sliderValx, posPruebax, posCel.latitude);
		sliderValy = GUI.HorizontalSlider (new Rect (width * relSliderPosW, height * relSliderPosH+relativeSliderH*height+5, 
		                                             relativeSliderW*width, relativeSliderH*height),
		                                   sliderValy, posPruebay, posCel.longitude);
		GUI.Label(new Rect (width * relSliderPosW, height * relSliderPosH-30, 80, 30) , posPruebax.ToString());
		GUI.Label(new Rect (width * relSliderPosW, height * relSliderPosH+relativeSliderH*height+5-30, 80, 30) , posPruebay.ToString());

		GUI.Label(new Rect (width * relSliderPosW+relativeSliderW*width-80, height * relSliderPosH-30, 80, 30) , posCel.latitude.ToString());
		GUI.Label(new Rect (width * relSliderPosW+relativeSliderW*width-80, height * relSliderPosH+relativeSliderH*height+5-30, 80, 30) , posCel.longitude.ToString());

		GUI.Label(new Rect (width * relSliderPosW+(relativeSliderW*width)/2-40, height * relSliderPosH-30, 80, 30) , sliderValx.ToString());
		GUI.Label(new Rect (width * relSliderPosW+(relativeSliderW*width)/2-40, height * relSliderPosH+relativeSliderH*height+5-30, 80, 30) , sliderValy.ToString());

		bool baux = Input.location.isEnabledByUser;
		GUI.Label (new Rect (width * relSliderPosW, height * 0.3f, 80, 30), baux.ToString ());

	}
}
