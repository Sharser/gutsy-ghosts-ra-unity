﻿using UnityEngine;
using System.Collections;

public class DistanciaAparicion : MonoBehaviour {

	public float relPosH;
	public float relPosW;
	public GUIText distDebug;
	public float distAparicion;
	public float distActivation;
	public GameObject compass;

	[SerializeField]
	private bool horario;
	private bool estaActivoComponentes;
	private float dist;
	private float posW;
	private float posH;
	private float PosTrackerX;
	private float PosTrackerY;
	private metaioTracker tracker;
	private LocationInfo posCel;

	// Use this for initialization

	void Start () {
		tracker = this.GetComponent<metaioTracker> ();
		tracker.setEnable (false);
		PosTrackerX = tracker.geoLocation.x;
		PosTrackerY = tracker.geoLocation.y;
		distDebug.pixelOffset = new Vector2 (Screen.width * relPosW, Screen.height * relPosH);
		compass.SetActive (false);
		estaActivoComponentes = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(horario){
			//DEBUG
			PosTrackerX = tracker.geoLocation.x;
			PosTrackerY = tracker.geoLocation.y;
			//
			posCel = Input.location.lastData;
			dist = calcularDistancia (new Vector2 (PosTrackerX, PosTrackerY), new Vector2 (posCel.latitude, posCel.longitude));
			if (dist <= distActivation) {
				compass.SetActive(true);
				activarMovGhost(true);
				if (dist <= distAparicion) {
					tracker.setEnable (true);
				} else {
					tracker.setEnable (false);
				}
			}else{
				compass.SetActive(false);
				activarMovGhost(false);
			}
			distDebug.text = dist.ToString ();
		}else{
			activarMovGhost(false);
			tracker.setEnable (false);
			compass.SetActive(false);
			distDebug.text = "";
		}
	}
	
	/// <summary>
	/// Activar el trackin por GPS si esta en el horario.
	/// </summary>
	/// <param name="enHora">Setea en <c>true</c> en se esta en hora.</param>
	public void activarGPSHorario(bool enHora){
		horario = enHora;
	}

	/// <summary>
	/// Calcula la distancia entre dos coordenadas.
	/// </summary>
	/// <returns>la distancia en metros aproximados</returns>
	/// <param name="Coor1">Coordenada1 (lat1, long1)</param>
	/// <param name="Coor2">Coordenada2 (lat2, long2).</param>
	private float calcularDistancia(Vector2 Coor1, Vector2 Coor2){
		float r = 6371000; //Radio de la tierra en metros
		float c = Mathf.PI / 180f;
		float seno1 = Mathf.Sin (c * (Coor2.x - Coor1.x) / 2f);
		float seno2 = Mathf.Sin (c * (Coor2.y - Coor1.y) / 2f);
		float d = 2 * r * Mathf.Asin (Mathf.Sqrt ( seno1*seno1 + Mathf.Cos (c * Coor1.x) * Mathf.Cos (c * Coor2.x) * seno2*seno2));
		return d;
	}

	private void activarMovGhost(bool activate){
		if(estaActivoComponentes == activate){
			return;
		}else{
			ActivarComp[] actCompChildrens = this.gameObject.GetComponentsInChildren<ActivarComp>();
			foreach (ActivarComp ac in actCompChildrens) {
				ac.activeAllComp(activate);
			}
			estaActivoComponentes = activate;
		}
	}
}
