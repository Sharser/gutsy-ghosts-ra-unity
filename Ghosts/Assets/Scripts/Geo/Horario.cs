﻿using System;
using UnityEngine;
using System.Collections;

public class Horario : MonoBehaviour {

	public int horaIni;
	public int minIni;
	public int horaFin;
	public int minFin;
	public float relPosH;
	public float relPosW;
	public GUIText horaTexto;
	
	private bool activarHora;
	private DateTime horaCel;
	private string menError = "Horarios no correctos";
	private DistanciaAparicion distAp;

	// Use this for initialization
	void Start () {
		verificarTiempo (horaIni, 23, 0);
		verificarTiempo (minIni, 59, 0);
		verificarTiempo (horaFin, 23, 0);
		verificarTiempo (minFin, 59, 0);
		compararTiempos ();
		horaTexto.pixelOffset = new Vector2 (Screen.width * relPosW, Screen.height * relPosH);
		distAp = this.GetComponent<DistanciaAparicion> ();
	}
	
	// Update is called once per frame
	void Update () {

		horaCel = System.DateTime.Now;
		string horaString= horaCel.Hour.ToString() + ":";
		if(horaCel.Minute < 10){
			horaString = horaString+"0"+horaCel.Minute.ToString();
		}else{
			horaString = horaString+horaCel.Minute.ToString();
		}
		horaTexto.text =  horaString;
		
		if(activarHora){
			if(horaIni <= horaCel.Hour && horaCel.Hour <= horaFin){
				if((horaIni == horaFin && minIni <= horaCel.Minute && horaCel.Minute  <= minFin) 
				   || (horaIni == horaCel.Hour && minIni <= horaCel.Minute)
				   || (horaFin == horaCel.Hour && horaCel.Minute <= minFin)
				   || (horaIni < horaCel.Hour && horaCel.Hour < horaFin)){
					distAp.activarGPSHorario(true);
				}else{
					distAp.activarGPSHorario(false);
				}
			}else{
				distAp.activarGPSHorario(false);
			}
		}else{
			distAp.activarGPSHorario(false);
		}

	}

	private void verificarTiempo(int tiempo, int max, int min){
			if(tiempo < min){
				tiempo = min;
			}else if(tiempo > max){
				tiempo = max;
			}
	}
	
	private void compararTiempos(){
		activarHora = true;
		if(horaIni > horaFin){
			horaTexto.text = menError;
			activarHora = false;
		}else if(horaIni == horaFin){
			if(minIni > minFin){
				horaTexto.text = menError;
				activarHora = false;
			}
		}

	}
}
