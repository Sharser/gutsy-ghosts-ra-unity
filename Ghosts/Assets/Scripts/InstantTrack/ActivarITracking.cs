﻿using UnityEngine;
using System.Collections;
using metaio;

public class ActivarITracking : MonoBehaviour {

	public float heightRel = 0.13f;
	public float widthRel = 0.125f;
	
	private float posH=0.85f;
	private float posW = 0.5f;
	private float height;
	private float width;
	private float botonh;
	private float botonw;

	// Use this for initialization
	void Start () {
		height = Screen.height;
		width = Screen.width;
		botonh = height * heightRel;
		botonw = width * widthRel;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI(){
		if(GUI.Button(new Rect(posW*width-(botonw*1.5f)-5f, posH*height, botonw, botonh),"2D"))
		{
			// start instant tracking, it will call the callback once done
			MetaioSDKUnity.startInstantTracking("INSTANT_2D", "");
			GUI.enabled = false;
		}
		
		if(GUI.Button(new Rect(posW*width-(botonw*0.5f), posH*height, botonw, botonh),"2D Rectified"))
		{
			#if UNITY_EDITOR || UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN
			Debug.LogError("This mode is currently not implemented for the Editor Preview and Windows/Mac Standalone, please try on a mobile device");
			#else
			// start instant tracking, it will call the callback once done
			MetaioSDKUnity.startInstantTracking("INSTANT_2D_GRAVITY", "");
			GUI.enabled = false;
			#endif
		}
		
		if(GUI.Button(new Rect(posW*width+(botonw*0.5f)+5f, posH*height, botonw, botonh),"3D"))
		{
			// start instant tracking, it will call the callback once done
			MetaioSDKUnity.startInstantTracking("INSTANT_3D", "");
			GUI.enabled = false;
		}
	}
}
