﻿using UnityEngine;
using System.Collections;

public class MovAparicionInst : MonoBehaviour {

	public float deepIni;
	public float deepEnd;
	public float spdAni;
	public float timeAparicion;

	[SerializeField]
	private bool activarAni = false;
	private bool aparecio;
	private float count;
	private TouchInstant touch;
	// Use this for initialization
	//Tiene que comezar en deepIni, el Respawner se encargara de eso.
	void Start () {
		/*Vector3 posAux = this.transform.position;
		posAux.y = deepIni;
		this.transform.position = posAux;*/
		aparecio = false;
		count = 0.0F;
		touch = GetComponent<TouchInstant> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(activarAni){
			if(!aparecio){
			   if(this.transform.position.y < deepEnd){
					this.transform.Translate(this.transform.forward*Time.deltaTime*spdAni);
				}else{
					aparecio = true;
					touch.setDestruir(true);
				}
			}else{
				if(count < timeAparicion){
					count += Time.deltaTime;
				}else{
					if(this.transform.position.y > deepIni){
						this.transform.Translate(-this.transform.forward*Time.deltaTime*spdAni);
					}else{
						GameObject.Destroy(this.gameObject);
					}
				}
			}
		}
	}

	public void setActivar(bool act){
		activarAni = act;
	}
}
