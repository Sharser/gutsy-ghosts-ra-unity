﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Respawner : MonoBehaviour {

	public float timeMax;
	public float timeMin;
	public float minX;
	public float maxX;
	public float minZ;
	public float maxZ;
	public float deepIniRespawn = -110f;
	public float deepEndRespawn = 0f;
	public float timeAparicion;
	public GameObject handPrefab;
	public float heightBotonRel = 0.13f;
	public float widthBotonRel = 0.125f;

	private bool trackEstabilizado;
	[SerializeField]
	private bool comenzar;
	private float count;
	private float timeSpawn;
	private float posH = 0.5f;
	private float posW = 0.5f;
	private float botonh;
	private float botonw;
	private metaioTracker tracker;
	[SerializeField]
	private List<MovAparicionInst> hands;
	// Use this for initialization
	void Start () {
		tracker = GetComponent<metaioTracker> ();
		hands = new List<MovAparicionInst> ();
		count = 0.0f;
		timeSpawn = Random.Range(timeMin, timeMax);
		trackEstabilizado = false;
		botonh = Screen.height * heightBotonRel;
		botonw = Screen.width * widthBotonRel;
	}
	
	// Update is called once per frame
	void Update () {
		if (tracker.isTracking > 0) {
			trackEstabilizado = true;
			if(comenzar){
				count += Time.deltaTime;
				Debug.Log(count);
				if(count > timeSpawn){
					respawn();
					timeSpawn = Random.Range(timeMin, timeMax);
					count = 0.0F;
				}
			}
		}else{
			trackEstabilizado = false;
			comenzar = false;
			reinicioHands();
		}
	}

	void OnGUI(){
		if(trackEstabilizado && !comenzar){
			if(GUI.Button(new Rect(posW*Screen.width-(botonw*0.5f), posH*Screen.height-(botonh*0.5f), botonw, botonh),"Start"))
			{
				comenzar = true;
			}
		}

	}

	private void respawn(){
		GameObject hand = Object.Instantiate(handPrefab) as GameObject;
		hand.transform.parent = this.transform;
		float xAux = Random.Range(minX, maxX);
		float zAux = Random.Range(minZ, maxZ);
		hand.transform.localPosition = new Vector3(xAux, deepIniRespawn, zAux);
		MovAparicionInst movAux = hand.GetComponent<MovAparicionInst>();
		movAux.deepIni = deepIniRespawn;
		movAux.deepEnd = deepEndRespawn;
		movAux.timeAparicion = timeAparicion;
		movAux.setActivar(true);
		hands.Add(movAux);
	}

	private void reinicioHands(){
		foreach(MovAparicionInst mai in hands){
			Destroy(mai.gameObject);
		}
		count = 0f;
		
	}

}
