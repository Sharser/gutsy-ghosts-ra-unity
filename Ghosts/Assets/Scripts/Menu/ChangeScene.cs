﻿using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	public float heightRel = 0.13f;
	public float widthRel = 0.125f;
	public GUIText gpsAlert;
	public int Aparicion = 1;
	public int Caras = 2;
	public int Geolicalizacion = 3;
	public int InstantTrack = 4;


	private float posH=0.0f;
	private float posW=0.5f;
	private float height;
	private float width;
	private float botonh;
	private float botonw;
	

	void Awake(){
		height = Screen.height;
		width = Screen.width;
		botonh = height * heightRel;
		botonw = width * widthRel;
		gpsAlert.enabled = false;
		gpsAlert.pixelOffset = new Vector2 (botonw/2+5f, 0f);

	}

	void OnGUI()
	{

		if (GUI.Button(new Rect(posW*width-(botonw/2), posH*height, botonw, botonh), "Aparicion"))
		{
			Application.LoadLevel(Aparicion);
		}

		if (GUI.Button(new Rect(posW*width-(botonw/2), posH*height+botonh+5, botonw, botonh), "Caras"))
		{
			Application.LoadLevel(Caras);
		}
		if (GUI.Button(new Rect(posW*width-(botonw/2), posH*height+(botonh+5)*2, botonw, botonh), "Geo"))
		{
			Input.location.Start (10f,10f);
			if(Input.location.isEnabledByUser && Input.location.lastData.latitude != 0f && Input.location.lastData.longitude != 0){
				Application.LoadLevel(Geolicalizacion);
			}else{
				gpsAlert.enabled = true;
			}
		}
		if (GUI.Button(new Rect(posW*width-(botonw/2), posH*height+(botonh+5)*3, botonw, botonh), "Instant"))
		{
				Application.LoadLevel(InstantTrack);
		}
	}
}